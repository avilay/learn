package net.avilaylabs.wiseguy;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.*;

import java.util.ArrayList;

/**
 * Created by avilay.parekh on 10/7/16.
 */
public class AppSpeechlet implements Speechlet {

    private static class Joke {

        private final String setup;
        private final String speechPunchline;
        private final String cardPunchline;

        Joke(String setup, String speechPunchline, String cardPunchline) {
            this.setup = setup;
            this.speechPunchline = speechPunchline;
            this.cardPunchline = cardPunchline;
        }
    }

    private static final String SESSION_STAGE = "stage";
    private static final String SESSION_JOKE_ID = "jokeid";
    private static final int KNOCK_KNOCK_STAGE = 1;
    private static final int SETUP_STAGE = 2;

    private static final ArrayList<Joke> JOKE_LIST = new ArrayList<Joke>();

    static {
        JOKE_LIST.add(new Joke("To", "Correct grammar is <break time=\"0.2s\" /> to whom.", "Correct grammar is 'to whom'."));
        JOKE_LIST.add(new Joke("Beets!", "Beats me!", "Beats me!"));
        JOKE_LIST.add(new Joke("Little old lady", "I didn't know you could yodel!", "I didn't know you could yodel!"));
        JOKE_LIST.add(new Joke("A broken pencil", "Never mind. It's pointless.", "Never mind. It's pointless."));
        JOKE_LIST.add(new Joke("Snow", "Snow use. I forgot.", "Snow use. I forgot."));
        JOKE_LIST.add(new Joke("Boo", "Aww <break time=\"0.3s\" /> it's okay <break time=\"0.3s\" /> don't cry.", "Aww, it's okay, don't cry."));
        JOKE_LIST.add(new Joke("Woo", "Don't get so excited, it's just a joke.", "Don't get so excited, it's just a joke."));
        JOKE_LIST.add(new Joke("Spell", "<say-as interpret-as=\"characters\">who</say-as>", "w.h.o"));
        JOKE_LIST.add(new Joke("Atch", "I didn't know you had a cold!", "I didn't know you had a cold!"));
        JOKE_LIST.add(new Joke("Owls", "Yes, they do.", "Yes, they do."));
        JOKE_LIST.add(new Joke("Berry", "Berry nice to meet you.", "Berry nice to meet you."));
    }

    @Override
    public void onSessionStarted(SessionStartedRequest req, Session sess) throws SpeechletException {}

    @Override
    public SpeechletResponse onLaunch(LaunchRequest req, Session sess) throws SpeechletException {
        return tellAJoke(sess);
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest req, Session sess) throws SpeechletException {
        Intent intentReq = req.getIntent();
        String intent = (intentReq != null) ? intentReq.getName() : null;

        if (intent.equals("TellMeAJokeIntent")) {
            return tellAJoke(sess);
        } else if (intent.equals("WhosThereIntent")) {
            return whosThere(sess);
        } else if (intent.equals("SetupNameWhoIntent")) {
            return nameWho(sess);
        } else if (intent.equals("AMAZON.HelpIntent")) {
            return help(sess);
        } else if (intent.equals("AMAZON.StopIntent") || intent.equals("AMAZON.CancelIntent")) {
            PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
            speech.setText("Goodbye");
            return SpeechletResponse.newTellResponse(speech);
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(SessionEndedRequest req, Session sess) throws SpeechletException {}

    private SpeechletResponse tellAJoke(Session sess) {
        String speech = "";

        // Reprompt speech will be triggered if the user doesn't respond.
        String repromptText = "You can ask, who's there";

        // Select a random joke and store it in the session variables
        int jokeID = (int) Math.floor(Math.random() * JOKE_LIST.size());

        // The stage variable tracks the phase of the dialogue.
        // When this function completes, it will be on stage 1.
        sess.setAttribute(SESSION_STAGE, KNOCK_KNOCK_STAGE);
        sess.setAttribute(SESSION_JOKE_ID, jokeID);
        speech = "Knock knock!";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Wise Guy");
        card.setContent(speech);

        SpeechletResponse response = newAskResponse(speech, false,
                repromptText, false);
        response.setCard(card);
        return response;
    }

    private SpeechletResponse whosThere(Session sess) {
        String speech = "", repromptText = "";
        if (sess.getAttributes().containsKey(SESSION_STAGE)) {
            if ((Integer) sess.getAttribute(SESSION_STAGE) == KNOCK_KNOCK_STAGE) {
                // Retrieve the joke's setup text.
                int jokeID = (Integer) sess.getAttribute(SESSION_JOKE_ID);
                speech = JOKE_LIST.get(jokeID).setup;

                // Advance the stage of the dialogue.
                sess.setAttribute(SESSION_STAGE, SETUP_STAGE);

                repromptText = "You can ask, " + speech + " who?";

            } else {
                sess.setAttribute(SESSION_STAGE, KNOCK_KNOCK_STAGE);
                speech = "That's not how knock knock jokes work! <break time=\"0.3s\" /> Knock knock";
                repromptText = "You can ask who's there.";
            }
        } else {
            // If the session attributes are not found, the joke must restart.
            speech =
                    "Sorry, I couldn't correctly retrieve the joke. You can say, tell me a joke.";
            repromptText = "You can say, tell me a joke.";
        }

        return newAskResponse("<speak>" + speech + "</speak>", true, repromptText, false);
    }

    private SpeechletResponse nameWho(Session sess) {
        String speechOutput = "", repromptText = "";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Wise Guy");

        if (sess.getAttributes().containsKey(SESSION_STAGE)) {
            if ((Integer) sess.getAttribute(SESSION_STAGE) == SETUP_STAGE) {
                int jokeID = (Integer) sess.getAttribute(SESSION_JOKE_ID);
                speechOutput = JOKE_LIST.get(jokeID).speechPunchline;
                card.setContent(JOKE_LIST.get(jokeID).cardPunchline);

                // Create the ssml text output
                SsmlOutputSpeech outputSpeech = new SsmlOutputSpeech();
                outputSpeech.setSsml("<speak>" + speechOutput + "</speak>");

                // If the joke completes successfully, this function will end the active session
                return SpeechletResponse.newTellResponse(outputSpeech, card);
            } else {
                sess.setAttribute(SESSION_STAGE, KNOCK_KNOCK_STAGE);
                speechOutput = "That's not how knock knock jokes work! <break time=\"0.3s\" /> Knock knock";
                repromptText = "You can ask who's there.";

                card.setContent("That's not how knock knock jokes work! Knock knock");

                // Create the ssml text output
                SsmlOutputSpeech outputSpeech = new SsmlOutputSpeech();
                outputSpeech.setSsml("<speak>" + speechOutput + "</speak>");
                PlainTextOutputSpeech repromptOutputSpeech = new PlainTextOutputSpeech();
                repromptOutputSpeech.setText(repromptText);
                Reprompt repromptSpeech = new Reprompt();
                repromptSpeech.setOutputSpeech(repromptOutputSpeech);

                // If the joke has to be restarted, then keep the session alive
                return SpeechletResponse.newAskResponse(outputSpeech, repromptSpeech, card);
            }
        } else {
            speechOutput =
                    "Sorry, I couldn't correctly retrieve the joke. You can say, tell me a joke";
            repromptText = "You can say, tell me a joke";
            card.setContent(speechOutput);
            SpeechletResponse response = newAskResponse(speechOutput, false,
                    repromptText, false);
            response.setCard(card);
            return response;
        }
    }

    private SpeechletResponse help(Session sess) {
        String text = "";
        int stage = -1;
        if (sess.getAttributes().containsKey(SESSION_STAGE)) {
            stage = (Integer) sess.getAttribute(SESSION_STAGE);
        }
        switch (stage) {
            case 0:
                text =
                        "Knock knock jokes are a fun call and response type of joke. "
                                + "To start the joke, just ask by saying tell me a"
                                + " joke, or you can say exit.";
                break;
            case 1:
                text = "You can ask, who's there, or you can say exit.";
                break;
            case 2:
                text = "You can ask, who, or you can say exit.";
                break;
            default:
                text =
                        "Knock knock jokes are a fun call and response type of joke. "
                                + "To start the joke, just ask by saying tell me a "
                                + "joke, or you can say exit.";
        }

        String reprompt = text;
        return newAskResponse(text, false, reprompt, false);
    }

    private SpeechletResponse newAskResponse(String text, boolean isOutputSsml,
                                             String repromptText, boolean isRepromptSsml) {
        OutputSpeech outputSpeech, repromptOutputSpeech;
        if (isOutputSsml) {
            outputSpeech = new SsmlOutputSpeech();
            ((SsmlOutputSpeech) outputSpeech).setSsml(text);
        } else {
            outputSpeech = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech) outputSpeech).setText(text);
        }

        if (isRepromptSsml) {
            repromptOutputSpeech = new SsmlOutputSpeech();
            ((SsmlOutputSpeech) repromptOutputSpeech).setSsml(repromptText);
        } else {
            repromptOutputSpeech = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech) repromptOutputSpeech).setText(repromptText);
        }
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(repromptOutputSpeech);
        return SpeechletResponse.newAskResponse(outputSpeech, reprompt);
    }


}
