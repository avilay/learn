package net.avilaylabs.wiseguy;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

public class App  extends SpeechletRequestStreamHandler {

    private static final Set<String> supportedApplicationIds = new HashSet<String>();
    static {
        supportedApplicationIds.add("amzn1.ask.skill.8bb05ffc-70d2-4c1a-bf90-d64d0598d76a");
    }

    public App() {
        super(new AppSpeechlet(), supportedApplicationIds);
    }

}