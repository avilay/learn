Home Page
=========
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit

Alexa dev dashboard
===================
https://developer.amazon.com/edw/home.html#/

Voice design reference
======================
Voice design handbook
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/alexa-skills-kit-voice-design-handbook

Defining the voice interface
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/defining-the-voice-interface

Supported phrase to begin a conversation
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/supported-phrases-to-begin-a-conversation

App testing
===========
Registering echos for testing (To add more echos, add them to your 'Household')
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/testing-an-alexa-skill#Registering%20Your%20Echo%20for%20Testing

Test cases
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/testing-an-alexa-skill#Testing%20and%20Debugging

ASK Interface Ref
=================
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/alexa-skills-kit-interface-reference