package net.avilaylabs.mathgame;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

public class App  extends SpeechletRequestStreamHandler {

    private static final Set<String> supportedApplicationIds = new HashSet<String>();
    static {
        supportedApplicationIds.add("amzn1.ask.skill.d40c6fbb-bdc3-485c-a814-c8b3b451b3e5");
    }

    public App() {
        super(new AppSpeechlet(), supportedApplicationIds);
    }

}