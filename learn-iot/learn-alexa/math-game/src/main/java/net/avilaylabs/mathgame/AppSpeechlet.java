package net.avilaylabs.mathgame;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by avilay.parekh on 10/8/16.
 */
public class AppSpeechlet implements Speechlet {

    static Map<String, String> operUtterances = new HashMap<>();

    static {
        operUtterances.put("multiplication", "times");
        operUtterances.put("addition", "plus");
    }

    @Override
    public void onSessionStarted(SessionStartedRequest req, Session sess) throws SpeechletException {}

    @Override
    public SpeechletResponse onLaunch(LaunchRequest req, Session sess) throws SpeechletException {
        return welcome(sess);
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest req, Session sess) throws SpeechletException {
        Intent intent = req.getIntent();
        if (intent.getName().equals("QuestionIntent")) {
            return question(intent, sess);
        } else if (intent.getName().equals("AnswerIntent")) {
            return answer(intent, sess);
        } else if (intent.getName().equals("AMAZON.HelpIntent")) {
            return help();
        } else if (intent.getName().equals("AMAZON.StopIntent") || intent.equals("AMAZON.CancelIntent")) {
            PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
            speech.setText("Goodbye");
            return SpeechletResponse.newTellResponse(speech);
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(SessionEndedRequest req, Session sess) throws SpeechletException {}

    private SpeechletResponse welcome(Session sess) {
        String text = "I can quiz you on multiplication or addition. Which one do you want?";

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt);
    }

    private SpeechletResponse help() {
        String text = "You can ask me to quiz you on multiplication or addition";
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);
        return SpeechletResponse.newTellResponse(speech);
    }

    private SpeechletResponse question(Intent intent, Session sess) {
        String oper = intent.getSlot("operation").getValue();
        int x = ThreadLocalRandom.current().nextInt(1, 12 + 1);
        int y = ThreadLocalRandom.current().nextInt(1, 12 + 1);

        sess.setAttribute("operation", oper);
        sess.setAttribute("x", Integer.valueOf(x));
        sess.setAttribute("y", Integer.valueOf(y));

        String text = String.format("What is %d %s %d?", x, operUtterances.get(oper), y);

        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt);
    }

    private SpeechletResponse answer(Intent intent, Session sess) throws SpeechletException {
        String answer = intent.getSlot("answer").getValue();
        String text = "";
        try {
            int ans = Integer.parseInt(answer);
            String oper = (String)sess.getAttribute("operation");
            int x = (Integer)sess.getAttribute("x");
            int y = (Integer)sess.getAttribute("y");
            sess.removeAttribute("x");
            sess.removeAttribute("y");
            int correctAns = Integer.MIN_VALUE;
            if (oper.equals("multiplication")) {
                correctAns = x * y;
            } else if (oper.equals("addition")) {
                correctAns = x + y;
            } else {
                throw new SpeechletException("Unknown operation");
            }

            if (ans == correctAns) {
                text += "Correct. ";
            } else {
                text += String.format("Incorrect. The correct answer is %d. ", correctAns);
            }
            x = ThreadLocalRandom.current().nextInt(1, 12 + 1);
            y = ThreadLocalRandom.current().nextInt(1, 12 + 1);

            sess.setAttribute("x", Integer.valueOf(x));
            sess.setAttribute("y", Integer.valueOf(y));

            text += String.format("What is %d %s %d?", x, operUtterances.get(oper), y);
        } catch (NumberFormatException e) {
            text = "That is not a number. Try again.";
        }
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt);
    }

}
