QuestionIntent Quiz me on {operation}
QuestionIntent Ask me {operation}
QuestionIntent {operation}

AnswerIntent {x} times {y} is {answer}
AnswerIntent {answer}
AnswerIntent {x} multiplied by {y} is {answer}
