package net.avilaylabs.helloalexa;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AppSpeechlet implements Speechlet {
    private static final Logger log = LoggerFactory.getLogger(AppSpeechlet.class);

    @Override
    public void onSessionStarted(SessionStartedRequest req, Session session) throws SpeechletException {
        log.info("onSessionStarted requestId={}, sessionId={}", req.getRequestId(), session.getSessionId());
    }

    @Override
    public SpeechletResponse onLaunch(LaunchRequest req, Session session) throws SpeechletException {
        log.info("onLaunch requestId={}, sessionId={}", req.getRequestId(), session.getSessionId());
        return welcome();
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest req, Session session) throws SpeechletException {
        log.info("onIntent requestId={}, sessionId={}", req.getRequestId(), session.getSessionId());

        Intent reqIntent = req.getIntent();
        String intent = (reqIntent != null) ? reqIntent.getName() : null;
        if (intent.equals("HelloWorldIntent")) {
            return helloWorld();
        } else if (intent.equals("AMAZON.HelpIntent")) {
            return help();
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    @Override
    public void onSessionEnded(SessionEndedRequest req, Session session) throws SpeechletException {
        log.info("onSessionEnded requestId={}, sessionId={}", req.getRequestId(), session.getSessionId());
    }

    private SpeechletResponse welcome() {
        String text = "Welcome to the Alexa Skills Kit, you can say hello";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("HelloWorld");
        card.setContent(text);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        // Create reprompt
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }

    private SpeechletResponse help() {
        String text = "You can say hello to me!";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("HelloWorld");
        card.setContent(text);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        // Create reprompt
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(speech);

        return SpeechletResponse.newAskResponse(speech, reprompt, card);
    }

    private SpeechletResponse helloWorld() {
        String text = "Hello world";

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("HelloWorld");
        card.setContent(text);

        // Create the plain text output.
        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
        speech.setText(text);

        return SpeechletResponse.newTellResponse(speech, card);
    }
}
