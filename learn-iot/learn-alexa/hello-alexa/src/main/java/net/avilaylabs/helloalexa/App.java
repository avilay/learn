package net.avilaylabs.helloalexa;

import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App  extends SpeechletRequestStreamHandler {

    private static final Set<String> supportedApplicationIds = new HashSet<String>();
    static {
        supportedApplicationIds.add("amzn1.ask.skill.3976e3d6-1c52-451c-a871-b9b95d12b488");
    }

    public App() {
        super(new AppSpeechlet(), supportedApplicationIds);
    }

}
