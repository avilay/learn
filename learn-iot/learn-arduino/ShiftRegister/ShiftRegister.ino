/*
The application of the shift register is to "multiplex" a single Arduino PIN into 8 PINS
on the shift register itself.

Conceptually it works based on 3 PINs - the clock pin, data pin, and the latch pin. Latch pin
is like a latch. You open the latch (set it to LOW), set the data pin with 8 bits, and then close the latch (set it to HIGH).
After the latch is closed, the 8 bits set on the data pins are sent to each of the "multiplexed" pin. The way to set the 8 bits
is to send each bit in between the clock pulses that are recvd by the clock pin. Fortunately, Arduino has a function called
shiftOut which takes care of interleaving the data and clock pulses.

Overall the shift register has 8 "multiplexed" PINs, 1 VIN, 1 GND, 1 data, 1 latch, and 1 clock pins. 
Apart from these 13 pins, it has 3 more pins - 
    - a serial out pin for serial comms between the shift register and Arduino (this is mostly not used)
    - a master reclear pin, which needs to be connected to Arduino's +5V supply (along with the VIN that is also connected to the +5V)
    - Output enable pin, which needs to be connected to Arduino GND (along with the GND pin)
    
Refer to http://arduino.cc/en/tutorial/ShiftOut for detailed description.
*/

int latchPin = 5; // ST_CP PIN 12 of the register 
int clockPin = 6;  // SH_CP PIN 11 of the register
int dataPin = 4;  // DS PIN 14 of the register

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  turnOff();
}

void cycle() {
  for (int i = 0; i < 8; i++) {
    byte led = 0;
    bitSet(led, i);  // Sets the ith bit from left, starting the numbering at 0
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST, led);
    digitalWrite(latchPin, HIGH);
    delay(500);
  } 
}

void turnLedOn(int num) {
  int led = 0;
  bitSet(led, num);
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, led);
  digitalWrite(latchPin, HIGH);
}

void turnOff() {
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, 0);
  digitalWrite(latchPin, HIGH);
}

void loop() {
  cycle();
}


