#include <Utils.h>

const int ANALOG_IN = 0;

void setup() {
	Utils::connectSerial();
	Serial.println("Hello World");
}

void loop() {
	int mv = Utils::analogReadMV(ANALOG_IN);
	Serial.println(mv);
	delay(1000);	
}