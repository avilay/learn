#include "Arduino.h"
#include "Utils.h"

void Utils::connectSerial() {
	Serial.begin(9600);
	while (! Serial);
	Serial.println("Serial link established");
}

int Utils::analogReadMV(int pin) {
	int val = analogRead(pin);
	return map(val, 0, 1023, 0, 5000);
}