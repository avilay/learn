#ifndef UTILS_H
#define UTILS_H

#include "Arduino.h"
#include "Utils.h"

class Utils {
public:
	static void connectSerial();
	static int analogReadMV(int pin);
};

#endif