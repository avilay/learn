#include "Button.h"

const int BUTTON = 7;
const int LED = 13;
Button btn(BUTTON);

void setup() {
	pinMode(LED, OUTPUT);
	digitalWrite(LED, LOW);
	btn.onClick(&blinkTwice);
	btn.onHold(&stayOn);
	btn.onRelease(&blinkFourTimes);
}

void loop() {
	btn.listen();
}

void blink(int num) {
	for (int i =0; i < num; i++) {
		digitalWrite(LED, HIGH);
		delay(250);
		digitalWrite(LED, LOW);
		delay(250);
	}
}

void blinkTwice() {
	blink(2);
}

void blinkFourTimes() {
	blink(4);
}

void stayOn() {
	digitalWrite(LED, HIGH);
}

// int BUTTON1 = 7;
// int BUTTON2 = 8;
// Button btn1(BUTTON1);
// Button btn2(BUTTON2);

// void setup() {
// 	Serial.begin(9600);
// 	while (! Serial);
// 	Serial.println("Serial link established");
	
// 	btn1.onClick(&clickHandler);
// 	btn1.onHold(&holdHandler);
// 	btn1.onRelease(&relHandler);

// 	btn2.onClick(&clickHandler);
// 	btn2.onHold(&holdHandler);
// 	btn2.onRelease(&relHandler);
// }

// void loop() {
// 	btn1.listen();
// 	btn2.listen();
// }

// void clickHandler() {
// 	Serial.println("Inside click handler");
// }

// void holdHandler() {
// 	Serial.println("Inside hold handler");
// }

// void relHandler() {
// 	Serial.println("Inside release handler");
// }