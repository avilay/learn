#ifndef BUTTON_H
#define BUTTON_H

#include "Arduino.h"


class Button {
public:
	Button(int pin);
	void onClick(void(*handler)());
	void onHold(void(*handler)());
	void onRelease(void(*handler)());
	void listen();
	
private:
	int _pin;
	int _prevVal;
	unsigned long _pressStartTime;
	void (*_clickHandler)();
	void (*_holdHandler)();
	void (*_relHandler)();
};

#endif