#include <Button.h>

const int BUTTON = 7;
const int LED = 13;
Button btn(BUTTON);

void setup() {
	pinMode(LED, OUTPUT);
	digitalWrite(LED, LOW);
	btn.onClick(&blinkTwice);
	btn.onHold(&stayOn);
	btn.onRelease(&blinkFourTimes);
}

void loop() {
	btn.listen();
}

void blink(int num) {
	for (int i =0; i < num; i++) {
		digitalWrite(LED, HIGH);
		delay(250);
		digitalWrite(LED, LOW);
		delay(250);
	}
}

void blinkTwice() {
	blink(2);
}

void blinkFourTimes() {
	blink(4);
}

void stayOn() {
	digitalWrite(LED, HIGH);
}
