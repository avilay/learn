#include "Arduino.h"
#include "Button.h"


void defaultHandler() {}

Button::Button(int pin) {
	pinMode(pin, INPUT);
	_pin = pin;
	_clickHandler = &defaultHandler;
	_holdHandler = &defaultHandler;
	_relHandler = &defaultHandler;
}

void Button::onClick(void(*handler)()) {
	_clickHandler = handler;
}

void Button::onHold(void(*handler)()) {
	_holdHandler = handler;
}

void Button::onRelease(void(*handler)()) {
	_relHandler = handler;
}

// The button is considered as "clicked" if press to release is less than 500ms
// Otherwise it is in "hold" state while pressed and "released" state when released.
void Button::listen() {
	int currVal = digitalRead(_pin);
	if (_prevVal == LOW && currVal == HIGH) {
		_pressStartTime = millis();
	} else if (_prevVal == HIGH && currVal == HIGH) {
		if (millis() - _pressStartTime > 500) {
			_holdHandler();
		}
	} else if (_prevVal == HIGH && currVal == LOW) {
		if (millis() - _pressStartTime > 500) {
			_relHandler();
		} else {
			_clickHandler();
		}
	}
	_prevVal = currVal;
}

