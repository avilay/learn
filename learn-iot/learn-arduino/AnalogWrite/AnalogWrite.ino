/*
While it is possible to do PWM on normal digital pins like 2, 7, or 8, Arduino provides pins 9, 10, 11, etc. where it does
digital PWM in the hardware thus simulating analog writes. These pins take in values from 0 to 255, 0 for 0V output and 255 for
a full 5V output.
*/

const int LED1 = 9;
const int LED2 = 10;

void setup() {
	pinMode(LED1, OUTPUT);
	pinMode(LED2, OUTPUT);
}

void loop() {
	analogWrite(LED1, 0.7*255);  // Set it at 70% brightness
	analogWrite(LED2, 0.2*255);  // Set it at 20% brightness
}
