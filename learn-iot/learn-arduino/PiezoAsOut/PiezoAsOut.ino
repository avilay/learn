#include <SerialIO.h>

const int SPEAKER_PIN = 9;
SerialIO sio;

int length = 15; // the number of notes
char song[] = "ccggaagffeeddc "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;

void setup() {
	pinMode(SPEAKER_PIN, OUTPUT);
	sio.connect();
}

void loop() {
	for (int i = 0; i < length; i++) {
		if (song[i] == ' ') {
			delay(beats[i] * tempo);
		} else {
			playTone(getToneForNote(song[i]), beats[i] * tempo);			
		}
		delay(tempo / 2);
	}
}

void playTone(int tone, int duration) {
	for (long i = 0; i < duration * 1000L; i += tone *2) {
		digitalWrite(SPEAKER_PIN, HIGH);
		delayMicroseconds(tone);
		digitalWrite(SPEAKER_PIN, LOW);
		delayMicroseconds(tone);
	}	
}

char notes[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };
int getToneForNote(char note) {
	for (int i = 0; i < 8; i++) {
		if (notes[i] == note) return tones[i];
	}
	return 1915;
}