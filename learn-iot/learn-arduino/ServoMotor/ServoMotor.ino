#include <Servo.h>

// TODO: Make this a part of a SerialIO library
int powerOfTen(int exponent) {
  if (exponent == 0) return 1;
  int val = 1;
  for(int i = 0; i < exponent; i++) {
    val = val * 10;  
  }
  return val;
}

int input(String prompt) {
  Serial.println(prompt);
  while (! Serial.available()) {
    delay(200); 
  }
  int buf[10];
  int len = 0;
  char c;
  boolean isNeg = false;
  while(Serial.available()) {
    c = Serial.read();
    if (c == '-') {
      isNeg = true;  
    } else {
      buf[len] = (int)(c - '0');
      len++;  
    }
  }
  int val = 0;
  for(int i = 0; i < len; i++) {
    val += buf[i] * powerOfTen(len - 1 - i);
  }
  if (isNeg) return val * -1;
  else return val;
}

int getint(String prompt, int lowerBound, int upperBound) {
  int val = input(prompt);
  while (val < lowerBound || val > upperBound) {
    Serial.print("Invalid entry: ");
    Serial.print(val);
    Serial.print(". Values have to be between ");
    Serial.print(lowerBound);
    Serial.print(" and ");
    Serial.println(upperBound);
    val = input(prompt);  
  }
  return val;
}

int getint(String prompt) {
  return input(prompt);  
}

void connectToSerial() {
  Serial.begin(9600);
  while(!Serial);
  Serial.println("Serial link established");
}

Servo myservo;
int pos  = 0;

void setup() {
  myservo.attach(9);
  connectToSerial();
}

void loop() {
  String prompt = "Enter degress to turn the Servo (-180 to 180):";
  int degreesToTurn = getint(prompt, -180, 180);
  String update = "Turning ";
  update += degreesToTurn;
  Serial.println(update);
  myservo.write(degreesToTurn);
  delay(20);
}

