#include <Utils.h>
#include <SerialIO.h>

int motorPin = 7;
SerialIO sio;

void setup() {
  sio.connect();
  pinMode(motorPin, OUTPUT);  
}

void loop() {
  //motorOnOrOff();
  //motorOnThenOff();
  //motorOnThenOffWithSpeed();
  analogWrite(motorPin, 0);
  int speed = sio.getint("Motor OFF. Enter speed to start the motor:");
  motorSpeed(speed);
}

void motorOnOrOff() {
  digitalWrite(motorPin, LOW);
  Serial.println("Motor OFF");
  int num = sio.getint("Enter any number to start the motor");
  digitalWrite(motorPin, HIGH);
  Serial.println("Motor ON");
  num = sio.getint("Enter any number to stop the motor");
}

void motorSpeed(int onSpeed) {
  Serial.print("Motor running at ");
  Serial.println(onSpeed);
  int onTime = 5000;
  analogWrite(motorPin, onSpeed);
  delay(onTime);
}

void motorOnThenOff() {   
  int onTime = 5000;
  int offTime = 5000;

  digitalWrite(motorPin, HIGH);
  Serial.println("Motor ON");
  delay(onTime);
  digitalWrite(motorPin, LOW);
  Serial.println("Motor OFF");
  delay(offTime);
}

void motorOnThenOffWithSpeed() {
  int onSpeed = 200;
  int onTime = 2500;
  
  int offSpeed = 50;
  int offTime = 1000;
  
  analogWrite(motorPin, onSpeed);
  delay(onTime);
  analogWrite(motorPin, offSpeed);
  delay(offTime);
}
