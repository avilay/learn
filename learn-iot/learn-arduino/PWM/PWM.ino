/*
Analog output can be simulated on most digital pins using Pulse Width Modulation. Say in a 100ms cycle, if I set the pin to HIGH (i.e., +5V)
for 20ms and to low (0V) for 80ms, and I do this really rapidly, it will *seem* as if the pin has a steady output of 0.2*5 = +1V.
Arduino has a special digital pins (marked with a ~) where this is done at the hardware level as part of the analogWrite call.
Check out AnanlogWrite project for examples.
*/

const int LED1 = 7;
const int LED2 = 8;
const int CYCLE_TIME = 10;

void setup() {
	pinMode(LED1, OUTPUT);
	pinMode(LED2, OUTPUT);
}

void loop() {
	pwmWrite(LED1, 0.7);  // Set it at 70% brightness
	pwmWrite(LED2, 0.2);  // Set it at 20% brightness
}

void pwmWrite(int pin, float width) {
	if (width < 0 || width > 1) {
		width = 0.5;
	}
	int onTime = width * CYCLE_TIME;
	int offTime = CYCLE_TIME - onTime;
	digitalWrite(pin, HIGH);
	delay(onTime);
	digitalWrite(pin, LOW);
	delay(offTime);
}