/*
Potentiometers are really vairable resistors. They use the principle of voltage division (V' = VR2/(R1 + R2)) to input different voltages into the
analog input pin. Twisting the dial changes the value of R1 and R2 thus changing V' which is connected to the A0 pin. The left pin is connected to 
+5 and right pin is connected to GND. The middle pin to A0.

When the dial is all the way to the left, i.e., R2 = R1, V' = V. So the reading will be 1023 => +5V. When the dial is all the way to the right,
i.e., R2 = 0, V' will read 0.
*/

const int POT_PIN = 0;  // A0

void setup() {
	Serial.begin(9600);
	while (! Serial);
	Serial.println("Serial link established");
}

void loop() {
	int val = analogRead(POT_PIN);
	Serial.println(val);
	delay(1000);
}