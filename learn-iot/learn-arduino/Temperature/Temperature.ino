#include <Utils.h>

const int ZERO_C_MV = 500;   // 0 deg C = 500 mV
const int MV_PER_DEG = 10;  // 10mV = 1 deg C

const int TEMP_PIN = 0;  // A0

void setup() {
	Utils::connectSerial();
}

void loop() {
	int mv = Utils::analogReadMV(TEMP_PIN);
	int degC = mVToCentigrade(mv);
	Serial.print(degC);
	Serial.println(" degress Centigrade");
	int degF = centigradeToFahrenheit(degC);
	Serial.print(degF);
	Serial.println(" degress Fahrenheit");
	delay(1000);
}

int mVToCentigrade(int mv) {
	return (mv - ZERO_C_MV) / MV_PER_DEG;
}

int centigradeToFahrenheit(int c) {
	return ((9*c)/5) + 32;
}