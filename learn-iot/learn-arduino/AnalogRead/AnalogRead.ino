/*
Arduino has 6 pins marked A0 to A5 that are capabale of analog input. Special pins are needed because unlike analog output 
which can be faked with digital PWM, analog input cannot be faked. The pins give out values from 0 to 1023. 0 when 0V is applied on it
and 1023 when a full 5V are applied on it.
*/

const int ANALOG_IN = 0;

void setup() {
	// Analog pins do not need a pinMode. They are automatically set up.
	Serial.begin(9600);
	while (! Serial);
	Serial.println("Serial link established");
}

void loop() {
	int val = analogRead(ANALOG_IN);
	Serial.println(val);
	delay(1000);
}