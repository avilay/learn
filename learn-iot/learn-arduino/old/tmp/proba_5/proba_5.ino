// these constants describe the pins. They won't change:
#include <Time.h>
#include <Math.h>

const int xpin = A0; // x-axis of the accelerometer
const int ypin = A2; // y-axis of the accelerometer
const int zpin = A1; // z-axis of the accelerometer 

int t1 = 0;
unsigned long prev = 0;
int i;

float D_X = 0; float D_Y = 0; float D_Z = 0;
float D_X0 = 0; float D_Y0 = 0; float D_Z0 = 0;
float Dist0 = 0;
float V_X0 = 0; float V_Y0 = 0; float V_Z0 = 0;
int FA = 0;

//const int sampleSize = 10;
  
void setup()
{
  // initialize the serial communications:
  Serial.begin(9600);
 
}

void loop()
{
  analogReference(EXTERNAL);

  unsigned long now = millis();
  float t = (now - prev) / 1000;
    
  int X = analogRead(xpin);
  int Y = analogRead(ypin);
  int Z = analogRead(zpin);
  
//  float Volt_X = (((X * 3.342) / 1023) - 0.034);
//  float Volt_Y = (((Y * 3.342) / 1023) - 0.035);
//  float Volt_Z = (((Z * 3.342) / 1023) - 0.033);
  
  float Cal_X = ((X - 511) / (1023 * 0.00390625));
  float Cal_Y = ((Y - 511) / (1023 * 0.00390625));
  float Cal_Z = ((Z - 511) / (1023 * 0.00390625));
  
  float V_X = V_X0 + Cal_X * (t - t1);
  float V_Y = V_Y0 + Cal_Y * (t - t1);
  float V_Z = V_Z0 + Cal_Z * (t - t1);
  
//  float G_X = ((Volt_X / 0.0065) - (1.629 / 0.0065));
//  float G_Y = ((Volt_Y / 0.0065) - (1.627 / 0.0065));
//  float G_Z = ((Volt_Z / 0.0065) - (1.632 / 0.0065)); 
  
  for (i = round (t); i < 1200; i = i + 1);
   
  float D_X = V_X + (0.5 * Cal_X * sq(t-t1));
  float D_Y = V_Y + (0.5 * Cal_Y * sq(t-t1));
  float D_Z = V_Z + (0.5 * Cal_Z * sq(t-t1));
  
  float Dif_X = (D_X - D_X0); 
  float Dif_Y = (D_Y - D_Y0);
  float Dif_Z = (D_Z - D_Z0);
    
  float Dist = sqrt (sq(Dif_X) + sq(Dif_Y) + sq(Dif_Z));
  Dist = (((Dist - Dist0)/2) + Dist0);
  
  int Xrot = round (atan2 (Dif_Y, Dif_Z) * 180 / 3.14159265 ); // radians to degrees and rounding
  int Yrot = round (atan2 (Dif_X, Dif_Z) * 180 / 3.14159265 );
  int Zrot = round (atan2 (Dif_Y, Dif_X) * 180 / 3.14159265 );
  if  (Cal_Z == 0)
  {
    FA = 0;
  }  
  else if (Cal_X == 0 && Cal_Y ==0 && Cal_Z > 0)
  {
    FA = 90;
  }
  else if (Cal_X == 0 && Cal_Y ==0 && Cal_Z < 0)
  {
    FA = -90;
  }
  else
  {
     float FlightAngle = ((Dif_Z) / (sqrt (sq (Dif_X) + sq (Dif_Y))));
     FA = round (atan (FlightAngle)  * (180 / 3.14159265)); // radioans to degree and rounding
  }
 
  
  D_X = ((Dif_X/2) + D_X0);
  D_Y = ((Dif_Y/2) + D_Y0);
  D_Z = ((Dif_Z/2) + D_Z0);
  
  D_X0 = D_X; D_Y0 = D_Y; D_Z0 = D_Z; t1 = t;
  V_X0 = V_X; V_Y0 = V_Y; V_Z0 = V_Z;
//  float Total_Gs = sqrt(sqrt(G_X)+ sqrt(G_Y) + sqrt(G_Z));
//  Serial.print (Volt_X);
//  Serial.print("\t");
//  Serial.print (Volt_Y);
//  Serial.print("\t");
//  Serial.print (Volt_Z);
//  Serial.print("\t");
  Serial.print(X);
  Serial.print("\t");
  Serial.print(Y);
  Serial.print("\t");
  Serial.print(Z);
  Serial.print("\t");
  Serial.print(Cal_X);
  Serial.print("\t"); 
  Serial.print(Cal_Y); 
  Serial.print("\t"); 
  Serial.print(Cal_Z); 
  Serial.print("\t");
  Serial.print(V_X); 
  Serial.print("\t");
  Serial.print(V_Y); 
  Serial.print("\t");
  Serial.print(V_Z); 
  Serial.print("\t"); 
  Serial.print(t);
  Serial.print("s");
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Dif_X);
  Serial.print(" m"); 
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Dif_Y);
  Serial.print(" m"); 
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Dif_Z);
  Serial.print(" m"); 
  Serial.print("\t");
  Serial.print("\t"); 
  Serial.print(Dist);
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Xrot);
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Yrot);
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(Zrot);
  Serial.print("\t");
  Serial.print("\t");
  Serial.print(FA);  
  Serial.print("\t");
  Serial.print("\t");
  
  
  Serial.println ();

  delay(1000);
   
}
