// these constants describe the pins. They won't change:
const int xpin = A3;                  // x-axis of the accelerometer
const int ypin = A2;                  // y-axis
const int zpin = A1;                  // z-axis (only on 3-axis models)

void setup()
{
  // initialize the serial communications:
  Serial.begin(9600);
 
}

void loop()
{
  
  int X = analogRead(xpin);
  int Y = analogRead(ypin);
  int Z = analogRead(zpin);
    
  X = map(X, 0, 1023, 0, 1023);
  Y = map(Y, 0, 1023, 0, 1023);
  Z = map(Z, 0, 1023, 0, 1023);
  
//  float Volt_X = (((X*2.6)/1023));
//  float Volt_Y = (((Y*2.6)/1023));
//  float Volt_Z = (((Z*2.6)/1023));
  
//  float Cal_X = 
//  float Cal_Y = (Volt_Y - 1.62)/1000;
//  float Cal_Z = (Volt_Z - 1.62)/1000;
  
  float G_X = ((X - 511) * 0.39100684);
  float G_Y = ((Y - 511) * 0.39100684);
  float G_Z = ((Z - 511) * 0.2); 
  
  float Total_Gs = sqrt(sqrt(G_X)+ sqrt(G_Y) + sqrt(G_Z));

 
//  Serial.print(Volt_X);
//  Serial.print("\t");
//  Serial.print(Volt_Y);
//  Serial.print("\t");
//  Serial.print(Volt_Z);
//  Serial.print("\t");
  Serial.print(X);
  Serial.print("\t");
  Serial.print(Y);
  Serial.print("\t");
  Serial.print(Z);
  Serial.print("\t");
  Serial.print(G_X);
  Serial.print("\t"); 
  Serial.print(G_Y); 
  Serial.print("\t"); 
  Serial.print(G_Z); 
  Serial.print("\t"); 
 
  Serial.println ();

  delay(2000);
   
}
