// The RGB LED that I have is of the "cathode" kind. Which means the longest end (which is second from the flat side) is
// the positive end. The three shorter ends are negative end of each of red, green, and blue
// LEDs inside the system. Each needs to be connected to GND for the LED to turn on.
// So if I want to turn the LED to green, the longest pin should be connected to the power supply, and the green pin should be connected to GND.
// However, remeber that the other 2 PINs will also be at 0 by default if they are connected to the Arduino. 
// So I have to ensure that they are max voltage.
// In a nutshell, in order to turn the LED green, I have to supply 255,0,255 voltage.
// This is a bit counter-intuitive to the RGB color scheme that is used elsewhere, where 0,255,0 is for green.
// To make this more intuitive, I have written the function setColor that will take the normal RGB code and then inverse the voltage.

int greenPin = 9;
int redPin = 10;
int bluePin = 11;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);  
}

void loop() {
  // Turn on green
//  analogWrite(greenPin, 0); 
//  analogWrite(redPin, 255);
//  analogWrite(bluePin, 255); 
  setColor(0, 255, 0);
  delay(1000);
  setColor(255, 0, 0);  // Red
  delay(1000);
  setColor(0, 0, 255);  // Blue
  delay(1000);
  setColor(231, 76, 60);  // Alizarin
  delay(1000);
  //rgb(142, 68, 173)
  setColor(142, 68, 173);  // Wisteria
  delay(1000);
}

void setColor(int r, int g, int b) {
  int rVolts = 255 - r;
  int gVolts = 255 - g;
  int bVolts = 255 - b;
  analogWrite(redPin, rVolts);
  analogWrite(greenPin, gVolts);
  analogWrite(bluePin, bVolts);  
}
