#include <Math.h>

double GSV = 6.5;  // Sensitivity from the datasheet in mV/G
double VZERO = 1500;  // Voltage emitted at 0G, zero g bias level in mV
int XZERO = 512;
int YZERO = 515;
int ZZERO = 510;

int XPIN = A0;
int YPIN = A1;
int ZPIN = A2;

void setup() {
  analogReference(EXTERNAL);
  Serial.begin(9600);
}

void loop() {
  int xRaw = readAdc(XPIN);
  int yRaw = readAdc(YPIN);
  int zRaw = readAdc(ZPIN);
  double xG = adcToG(xRaw, XZERO);
  double yG = adcToG(yRaw, YZERO);
  double zG = adcToG(zRaw, ZZERO);
  double pitch = calcPitch(xG, yG, zG);
  double roll = calcRoll(xG, zG);
  
  Serial.print("Raw Values: X = ");
  Serial.print(xRaw);
  Serial.print(" ");
  Serial.print("Y = ");
  Serial.print(yRaw);
  Serial.print(" ");
  Serial.print("Z = ");
  Serial.println(zRaw);
  Serial.print("G Values: X = ");
  Serial.print(xG);
  Serial.print(" ");
  Serial.print("Y = ");
  Serial.print(yG);
  Serial.print(" ");
  Serial.print("Z = ");
  Serial.println(zG);
  Serial.print("Pitch: ");
  Serial.print(pitch);
  Serial.print(" ");
  Serial.print("Roll: ");
  Serial.println(roll);
  Serial.println(" ");
  delay(500);
}

int readAdc(int pin) {
  return analogRead(pin);  
}

double adcToG(int bits, int bitszero) {
  double voltsPerBit = VZERO/bitszero;
  double v = bits * voltsPerBit;
  double deltaFromZeroV = v - VZERO;
  return deltaFromZeroV/GSV;
}

double calcRoll(double xG, double zG) {
  return atan2((-1*xG), zG);  
}

double calcPitch(double xG, double yG, double zG) {
  return atan2(yG, sqrt(sq(xG) + sq(zG))); 
}
