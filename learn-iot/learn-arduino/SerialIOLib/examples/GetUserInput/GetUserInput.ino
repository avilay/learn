#include <SerialIO.h>

SerialIO sio;

void setup() {
	sio.connect();
}

void loop() {
	int num = sio.getint("Enter an integer:");
	Serial.print("You entered ");
	Serial.println(num);

	String str = sio.getString("Enter a string:");
	Serial.print("You entered ");
	Serial.println(str);

	float fnum = sio.getfloat("Enter a float between -1.5 to +1.5:", -1.5, 1.5);
	Serial.print("You entered ");
	Serial.println(fnum);
}