#ifndef SerialIO_h
#define SerialIO_h

#include "Arduino.h"

class SerialIO {
public:
	SerialIO();
	void connect();
	int getint(String prompt);
	int getint(String prompt, int lower, int upper);
	float getfloat(String prompt);
	float getfloat(String prompt, float lower, float upper);
	String getString(String prompt);	

private:
	String _prompt;
	void waitForInput();
};

#endif