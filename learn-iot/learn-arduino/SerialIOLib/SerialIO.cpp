#include "Arduino.h"
#include "SerialIO.h"

SerialIO::SerialIO() {}

void SerialIO::connect() {
	Serial.begin(9600);
	while (! Serial);
	Serial.println("Serial link established");
}

void SerialIO::waitForInput() {
	Serial.println(_prompt);
	// Wait for the user to start typing
	while (! Serial.available()) {
		delay(200);
	}
}

int SerialIO::getint(String prompt) {
	_prompt = prompt;
	waitForInput();
	return Serial.parseInt();
}

int SerialIO::getint(String prompt, int lower, int upper) {
	int num = getint(prompt);
	while (num < lower || num > upper) {
		Serial.print("Invalid entry. You must enter a number between ");
		Serial.print(lower);
		Serial.print(" and ");
		Serial.println(upper);
		num = getint(prompt);
	}
	return num;
}

float SerialIO::getfloat(String prompt) {
	_prompt = prompt;
	waitForInput();
	return Serial.parseFloat();
}

float SerialIO::getfloat(String prompt, float lower, float upper) {
	float num = getfloat(prompt);
	while (num < lower || num > upper) {
		Serial.print("Invalid entry. You must enter a number between ");
		Serial.print(lower);
		Serial.print(" and ");
		Serial.println(upper);
		num = getfloat(prompt);	
	}
	return num;
}

String SerialIO::getString(String prompt) {
	_prompt = prompt;
	waitForInput();
	int currBytesRead = 0;
	int prevBytesRead = 0;
	unsigned long startRead = millis();
	// Wait for the user to stop typing
	while (millis() - startRead < 200) {
		currBytesRead = Serial.available();
		if (currBytesRead > prevBytesRead) {
			startRead = millis();
			prevBytesRead = currBytesRead;
		}
	}
	char *letters = new char[currBytesRead + 1];
	Serial.readBytes(letters, currBytesRead);
	letters[currBytesRead] = '\0';
	return String(letters);
}
