﻿using System;

using Microsoft.Quantum.Simulation.Core;
using Microsoft.Quantum.Simulation.Simulators;

namespace Scratch {
    class Driver {
        static void Main(string[] args) {
            using (var qsim = new QuantumSimulator()) {
                // HelloQ.Run(qsim).Wait();
                Scratch.Run(qsim).Wait();
            }
        }
    }
}