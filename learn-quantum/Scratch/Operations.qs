﻿namespace Scratch
{
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Intrinsic;
    open Microsoft.Quantum.Arrays;
    open Microsoft.Quantum.Convert;
    open Microsoft.Quantum.Diagnostics;
    open Microsoft.Quantum.Math;
    open Microsoft.Quantum.Arithmetic;
    open Microsoft.Quantum.Measurement;

    operation HelloQ () : Unit {
        Message("Hello quantum world!");
    }

    operation Scratch() : Unit {
        mutable numOnes = 0;
        for (i in 0..999) {
            using (q = Qubit()) {
                // Prepare the |-⟩ state.
                X(q);  // Remove this line to prepare in |+⟩ state
                H(q);
                let res = MeasureWithScratch([PauliX], [q]);
                if (res == One) {
                    set numOnes += 1;
                }
                Reset(q);
            }
        }
        let numZeros = 1000 - numOnes;
        Message($"numZeros={numZeros}  numOnes={numOnes}");
    }

    // operation Scratch(): Unit {
    //     using (q = Qubit()) {
    //         mutable numOnes = 0;
    //         for (i in 0..1000) {
    //             // H(q);
    //             X(q);
    //             let res = MeasureWithScratch([PauliZ], [q]);
    //             if (res == One) {
    //                 set numOnes += 1;
    //             }
    //         }
    //         let numZeros = 1000 - numOnes;
    //         Message($"numZeros={numZeros}  numOnes={numOnes}");
    //         Reset(q);
    //     }
    // }
}
