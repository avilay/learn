from pyspark.sql import SparkSession


def main():
    """Help for this script. Here is where I explain what arg is doing."""
    pqfile = "s3://avilabs-mldata-us-west-2/criteo/kaggle/train_0.parquet"
    spark = SparkSession.builder.getOrCreate()
    sdf = spark.read.parquet(pqfile)
    desc = sdf.select("i1", "i2", "i3").describe().show()
    # desc.write.mode("overwrite").csv(
    #     "s3://avilabs-mldata-us-west-2/scratch/output/criteo-metadata.csv"
    # )
    print("Describe job completed.")


if __name__ == "__main__":
    main()
