package net.avilabs.protojava;

import net.avilabs.protojava.AddressBookProtos.*;

public class ProtoBytes {
    public static byte[] write() {
        AddressBook addrBook = Proto.build();
        return addrBook.toByteArray();
    }

    public static void read(byte[] obj) throws Exception {
        AddressBook addrBook = AddressBook.parseFrom(obj);
        Proto.print(addrBook);
    }

    public static void main(String[] args) throws Exception {
        byte[] obj = write();
        read(obj);
    }
}
