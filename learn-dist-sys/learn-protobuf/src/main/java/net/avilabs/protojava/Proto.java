package net.avilabs.protojava;

import net.avilabs.protojava.AddressBookProtos.*;

public class Proto {
    public static AddressBook build() {
        Person.Builder person = Person.newBuilder();
        person.setId(1);
        person.setName("Cookie Monster");
        person.setEmail("cookie@monster.com");

        Person.PhoneNumber.Builder homePhone = Person.PhoneNumber.newBuilder();
        homePhone.setNumber("206-617-3488");
        homePhone.setType(Person.PhoneType.HOME);
        person.addPhone(homePhone);

        Person.PhoneNumber.Builder workPhone = Person.PhoneNumber.newBuilder();
        workPhone.setNumber("777-888-9999");
        workPhone.setType(Person.PhoneType.WORK);
        person.addPhone(workPhone);

        AddressBook.Builder addrBook = AddressBook.newBuilder();
        addrBook.addPerson(person);

        return addrBook.build();
    }

    public static void print(AddressBook addrBook) {
        addrBook.getPersonList().forEach(person -> {
            System.out.println("Person ID: " + person.getId());
            System.out.println("  Name: " + person.getName());
            if (person.hasEmail()) {
                System.out.println("  E-mail address: " + person.getEmail());
            }

            for (Person.PhoneNumber phoneNumber : person.getPhoneList()) {
                switch (phoneNumber.getType()) {
                    case MOBILE:
                        System.out.print("  Mobile phone #: ");
                        break;
                    case HOME:
                        System.out.print("  Home phone #: ");
                        break;
                    case WORK:
                        System.out.print("  Work phone #: ");
                        break;
                }
                System.out.println(phoneNumber.getNumber());
            }
        });
    }
}
