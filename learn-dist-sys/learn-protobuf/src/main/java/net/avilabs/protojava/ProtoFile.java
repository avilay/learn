package net.avilabs.protojava;

import net.avilabs.protojava.AddressBookProtos.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ProtoFile {
    public static void write() throws Exception {
        String fileName = "/Users/avilay.parekh/tmp/addrbook.protobuf";
        AddressBook addrBook = Proto.build();
        FileOutputStream ostream = new FileOutputStream(fileName);
        addrBook.writeTo(ostream);
        ostream.close();
    }

    public static void read() throws Exception {
        String fileName = "/Users/avilay.parekh/tmp/addrbook.protobuf";
        AddressBook addrBook = AddressBook.parseFrom(new FileInputStream(fileName));
        Proto.print(addrBook);
    }

    public static void main(String[] args) throws Exception {
        write();
        read();
    }
}
