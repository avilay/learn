import time
import zmq

print(zmq.zmq_version(), zmq.pyzmq_version())

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('tcp://*:5555')

while True:
    message = socket.recv()
    print('Received request {}'.format(message))
    time.sleep(1)
    socket.send(b'World')