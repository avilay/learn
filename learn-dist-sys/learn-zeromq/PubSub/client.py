import sys
import zmq

context = zmq.Context()
socket = context.socket(zmq.SUB)

print('Collecting updates from weather server...')
socket.connect('tcp://localhost:5556')

zip_filter = '98005'
socket.setsockopt_string(zmq.SUBSCRIBE, zip_filter)

total_temp = 0
for update_nbr in range(5):
    val = socket.recv_string()
    zipcode, temp, hum = val.split()
    total_temp += int(temp)

print('Avg temp for zipcode {} was {}'.format(zip_filter, total_temp / update_nbr))