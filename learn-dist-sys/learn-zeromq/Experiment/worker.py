import time
import zmq

context = zmq.Context()

sched_addr = context.socket(zmq.PULL)
sched_addr.connect('tcp://localhost:8888')

while True:
    print('Waiting for work from scheduler')
    load = int(sched_addr.recv())
    print('Working on load {}'.format(load))
    time.sleep(load)
