import zmq

context = zmq.Context()
my_addr = context.socket(zmq.PUSH)
my_addr.bind('tcp://*:8888')

while True:
    load = input('Enter workload: ')
    my_addr.send_string(load)