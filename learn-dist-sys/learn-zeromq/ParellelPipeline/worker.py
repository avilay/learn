import sys
import time
import zmq

context = zmq.Context()

receiver = context.socket(zmq.PULL)
receiver.connect('tcp://localhost:5557')

sender = context.socket(zmq.PUSH)
sender.connect('tcp://localhost:5558')

while True:
    print('Waiting for Ventilator to send work')
    s = receiver.recv()

    # Simple progress indicator
    sys.stdout.write('.')
    sys.stdout.flush()

    # Do the "work"
    time.sleep(int(s) * 0.001)

    # Send the "results"
    sender.send(b'')
