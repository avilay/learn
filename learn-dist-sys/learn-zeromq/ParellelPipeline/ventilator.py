import zmq
import random
import time

context = zmq.Context()
sender = context.socket(zmq.PUSH)
sender.bind('tcp://*:5557')

sink = context.socket(zmq.PUSH)
sink.connect('tcp://localhost:5558')

input('Press Enter when workers are ready: ')

# This first message is 0 and signals start of batch
sink.send(b'0')

# Send 100 tasks
total_msec = 0
for task_nbr in range(100):
    workload = random.randint(1, 100)
    total_msec += workload
    sender.send_string('{}'.format(workload))

print('Total expected cost: {} msec'.format(total_msec))
time.sleep(1)
