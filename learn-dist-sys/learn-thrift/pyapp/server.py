import time

from .tutorial import Calculator
from .tutorial.ttypes import InvalidOperation, Operation

from .shared.ttypes import SharedStruct

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer


class CalculatorHandler:
    def __init__(self):
        self.log = {}

    def ping(self):
        print("Ping")

    def add(self, n1, n2):
        ans = n1 + n2
        print(f"add({n1}, {n2}) = {ans}")
        return ans

    def calculate(self, logid, work):
        print(f"calculate({logid}, {work})")
        if work.op == Operation.ADD:
            val = work.num1 + work.num2
        elif work.op == Operation.SUBTRACT:
            val = work.num1 - work.num2
        elif work.op == Operation.MULTIPLY:
            val = work.num1 * work.num2
        elif work.op == Operation.DIVIDE:
            if work.num2 == 0:
                err = InvalidOperation()
                err.whatOp = work.op
                err.why = "Cannot divide by 0"
                raise err
            val = work.num1 / work.num2
        else:
            err = InvalidOperation()
            err.whatOp = work.op
            err.why = "Unknown operation"
            raise err

        log = SharedStruct()
        log.key = logid
        log.value = str(val)
        self.log[logid] = log

        return val

    def getStruct(self, key):
        print(f"getStruct({key})")
        return self.log[key]

    def fireAndForget(self):
        print("fireAndForget start")
        time.sleep(3)
        print("fireAndForget end")


def main():
    handler = CalculatorHandler()
    processor = Calculator.Processor(handler)

    transport = TSocket.TServerSocket(host="127.0.0.1", port=9090)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)


if __name__ == "__main__":
    main()
