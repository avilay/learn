#include <raylib.h>
#include <string>
#include <unordered_map>
#include <vector>

struct BackgroundTextures {
  int screenWidth;
  int screenHeight;

  Texture2D background;
  float backgroundScrollSpeed;
  float backgroundDelta;

  Texture2D midground;
  float midgroundScrollSpeed;
  float midgroundDelta;

  Texture2D foreground;
  float foregroundScrollSpeed;
  float foregroundDelta;

  BackgroundTextures(int screenWidth, int screenHeight) : screenWidth(screenWidth), screenHeight(screenHeight) {
    backgroundScrollSpeed = 0.0f;
    midgroundScrollSpeed = 0.0f;
    foregroundScrollSpeed = 0.0f;
    foregroundDelta = 0.0f;
    midgroundDelta = 0.0f;
    backgroundDelta = 0.0f;
  }

  ~BackgroundTextures() {
    UnloadTexture(background);
    UnloadTexture(midground);
    UnloadTexture(foreground);
  }

  void addBackground(const std::string& filename, const float scrollSpeed) {
    // TODO: Check that the file is wider than the screen width
    background = LoadTexture(filename.c_str());
    backgroundScrollSpeed = scrollSpeed;
  }

  void addMidground(const std::string& filename, const float scrollSpeed) {
    // TODO: Check that the file is wider than the screen width
    midground = LoadTexture(filename.c_str());
    midgroundScrollSpeed = scrollSpeed;
  }

  void addForeground(const std::string& filename, const float scrollSpeed) {
    // TODO: Check that the file is wider than the screen width
    foreground = LoadTexture(filename.c_str());
    foregroundScrollSpeed = scrollSpeed;
  }

  float getBackgroundPosition() {
    backgroundDelta -= backgroundScrollSpeed;
    if (backgroundDelta <= static_cast<float>(-background.width) * 2) {
      backgroundDelta = 0;
    }
    return backgroundDelta;
  }

  float getMidgroundPosition() {
    midgroundDelta -= midgroundScrollSpeed;
    if (midgroundDelta <= static_cast<float>(-foreground.width) * 2) {
      midgroundDelta = 0;
    }
    return midgroundDelta;
  }

  float getForegroundPosition() {
    foregroundDelta -= foregroundScrollSpeed;
    if (foregroundDelta <= static_cast<float>(-foreground.width) * 2) {
      foregroundDelta = 0;
    }
    return foregroundDelta;
  }

  void drawTexture(const Texture2D& texture, const float pos) {
    DrawTextureEx(texture, (Vector2){.x=pos, .y=20}, 0.0f, 2.0f, WHITE);

    const float extPos = static_cast<float>(texture.width) * 2 + pos;
    DrawTextureEx(texture, (Vector2){.x=extPos, .y=20}, 0.0f, 2.0f, WHITE);
  }

  void draw() {
    BeginDrawing();
    ClearBackground(GetColor(0x052c46ff));
    drawTexture(background, getBackgroundPosition());
    drawTexture(midground, getMidgroundPosition());
    drawTexture(foreground, getForegroundPosition());
  }

};

int main() {
  constexpr int screenWidth = 800;
  constexpr int screenHeight = 450;

  InitWindow(screenWidth, screenHeight, "Background Demo");
  BackgroundTextures textures(screenWidth, screenHeight);
  SetTargetFPS(60);

  while (!WindowShouldClose()) {
    update_draw_frame();
  }

  CloseWindow();
  return 0;
}

void load_textures() {
  background = LoadTexture("/home/avilay/projects/bitbucket/learn/learn-gfx/learn-raylib/web/resources/cyberpunk_street_background.png");
  midground = LoadTexture("/home/avilay/projects/bitbucket/learn/learn-gfx/learn-raylib/web/resources/cyberpunk_street_midground.png");
  foreground = LoadTexture("/home/avilay/projects/bitbucket/learn/learn-gfx/learn-raylib/web/resources/cyberpunk_street_foreground.png");
}

void update_draw_frame() {
  scrolling_back -= 0.01f;
  scrolling_mid -= 0.5f;
  scrolling_fore -= 1.0f;

  // All the textures will be scaled to 2x which is why the width is multiplied by 2
  if (scrolling_back <= static_cast<float>(-background.width) * 2) scrolling_back = 0;
  if (scrolling_mid <= static_cast<float>(-midground.width) * 2) scrolling_mid = 0;
  if (scrolling_fore <= static_cast<float>(-foreground.width) * 2) scrolling_fore = 0;

  BeginDrawing();
  ClearBackground(GetColor(0x052c46ff));

  // Stitch two copies of the texture one after the other (along the positive x-axis)
  // Each copy is scaled by 2x
  DrawTextureEx(
    background,
    (Vector2){.x=scrolling_back, .y=20},
    0.0f,
    2.0f,
    WHITE
  );
  DrawTextureEx(
    background,
    (Vector2){.x=static_cast<float>(background.width) * 2 + scrolling_back, .y=20},
    0.0f,
    2.0f,
    WHITE
  );

  DrawTextureEx(
    midground,
    (Vector2){.x=scrolling_mid, .y=20},
    0.0f,
    2.0f,
    WHITE
  );
  DrawTextureEx(
    midground,
    (Vector2){.x=static_cast<float>(midground.width) * 2 + scrolling_mid, .y=20},
    0.0f,
    2.0f,
    WHITE
  );

  DrawTextureEx(
    foreground,
    (Vector2){.x=scrolling_fore, .y=70},
    0.0f,
    2.0f,
    WHITE
  );
  DrawTextureEx(
    foreground,
    (Vector2){.x=static_cast<float>(foreground.width) * 2 + scrolling_fore, .y=70},
    0.0f,
    2.0f,
    WHITE
  );

  EndDrawing();
}