import requests
from aiohttp import web

PORT = 4390
CLIENT_ID = '394030541478.394208661862'
CLIENT_SECRET = '10e08f317a88f9530e5513f238f5f590'

async def handle(req):
    msg = 'Ngrok is working! - Path Hit: ' + req.url.human_repr()
    return web.Response(text=msg)


async def oauth(req):
    if 'code' not in req.query:
        raise web.HTTPInternalServerError('code was not found!')
    payload = {
        'code': req.query['code'],
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET
    }
    resp = requests.get('https://slack.com/api/oauth.access', params=payload)
    if resp.status_code == 200:
        print('Got back a success response from slack oauth')
        print(resp.json())
        return web.json_response(resp.json())
    else:
        raise web.HTTPInternalServerError('slack oauth failed!')


async def command(req):
    return web.Response(text='Your ngrok tunnel is up and running!')


app = web.Application()
app.add_routes([
    web.get('/', handle),
    web.post('/command', command),
    web.get('/oauth', oauth)
])

web.run_app(app, port=PORT)
