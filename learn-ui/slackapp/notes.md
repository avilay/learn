/todo @manjit Print out documents
Assign task "Print out documents" to user @manjit

/todos @avilay
List all the incomplete todos for user @avilay

/mytodos
List all of my incomplete todos

/todos
List all incomplete todos

/done 1
Mark task with id 1 as done