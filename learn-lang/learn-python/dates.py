from datetime import datetime, timezone
import pytz

localtime = datetime.now(timezone.utc).astimezone(pytz.timezone('US/Pacific')).isoformat()
print(localtime)
